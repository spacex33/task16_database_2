CREATE DATABASE IF NOT EXISTS student_grades;
USE student_grades;

CREATE TABLE IF NOT EXISTS student_grades.group (
  id INT NOT NULL,
  group_code VARCHAR(45) NOT NULL,
  specialty VARCHAR(45) NULL,
  PRIMARY KEY (id));

CREATE TABLE IF NOT EXISTS student (
  id INT NOT NULL,
  group_id INT NULL,
  first_name VARCHAR(45) NOT NULL,
  last_name VARCHAR(45) NOT NULL,
  pic_url VARCHAR(100) NULL,
  autobiography VARCHAR(300) NULL,
  birth_date DATE NOT NULL,
  home_address VARCHAR(45) NOT NULL,
  entrance_date DATE NULL,
  scholarship VARCHAR(45) NULL,
  rating VARCHAR(45) NULL,
  PRIMARY KEY (id),
  INDEX fk_student_group1_idx (group_id ASC),
  CONSTRAINT fk_student_group1
    FOREIGN KEY (group_id)
    REFERENCES student_grades.group (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
	
CREATE TABLE IF NOT EXISTS professor (
  id INT NOT NULL,
  first_name VARCHAR(45) NOT NULL,
  last_name VARCHAR(45) NOT NULL,
  PRIMARY KEY (id));

CREATE TABLE IF NOT EXISTS subject (
  id INT NOT NULL,
  professor_id INT NULL,
  name VARCHAR(45) NOT NULL,
  semester_number INT NULL,
  PRIMARY KEY (id),
  INDEX fk_subject_professor1_idx (professor_id ASC),
  CONSTRAINT fk_subject_professor1
    FOREIGN KEY (professor_id)
    REFERENCES student_grades.professor (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE IF NOT EXISTS grades (
  id INT NOT NULL,
  student_id INT NOT NULL,
  subject_id INT NOT NULL,
  result_module_1 DOUBLE NULL,
  result_module_2 DOUBLE NULL,
  exam_type VARCHAR(45) NULL,
  100_scale_semester_grade INT NULL,
  5_scale_semester_grade INT NULL,
  PRIMARY KEY (id, student_id, subject_id),
  INDEX fk_grades_student1_idx (student_id ASC),
  INDEX fk_grades_subject1_idx (subject_id ASC),
  CONSTRAINT fk_grades_student1
    FOREIGN KEY (student_id)
    REFERENCES student_grades.student (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_grades_subject1
    FOREIGN KEY (subject_id)
    REFERENCES student_grades.subject (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE IF NOT EXISTS student_has_subject (
  student_id INT NOT NULL,
  subject_id INT NOT NULL,
  PRIMARY KEY (student_id, subject_id),
  INDEX fk_student_has_subject_subject1_idx (subject_id ASC),
  INDEX fk_student_has_subject_student1_idx (student_id ASC),
  CONSTRAINT fk_student_has_subject_student1
    FOREIGN KEY (student_id)
    REFERENCES student_grades.student (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_student_has_subject_subject1
    FOREIGN KEY (subject_id)
    REFERENCES student_grades.subject (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
	

INSERT INTO professor VALUES(1, 'Sobolev', 'Andriy');
INSERT INTO professor VALUES(2, 'Boyko', 'Vasyl');
INSERT INTO professor VALUES(3, 'Ivanenko', 'Mariya');

INSERT INTO subject VALUES(1, 1, 'Data security', 2);
INSERT INTO subject VALUES(2, 3, 'Web development', 2);
INSERT INTO subject VALUES(3, 2, 'Politology', 2);
	
INSERT INTO student_grades.group VALUES(1, '544-2', 'Software engineering');
INSERT INTO student_grades.group VALUES(2, '444-1', 'Computer science');

INSERT INTO student VALUES(1, 1, 'Prohorov', 'Stas', 'url', 'url', '1997-11-18', 'Chernivtsi, Golovna str. 168', '2018-09-01', NULL, '6');
INSERT INTO student VALUES(2, 1, 'Dazyk', 'Oleksiy', 'url', 'url', '1997-06-28', 'Chernivtsi, Shevchenka str. 6', '2018-09-01', '1200', '2');
INSERT INTO student VALUES(3, 2, 'Sokolenko', 'Olena', 'url', 'url', '1998-11-18', 'Chernivtsi, Gonchara str. 48', '2015-09-01', NULL, '9');
INSERT INTO student VALUES(4, 2, 'Popov', 'Ihor', 'url', 'url', '1998-11-18', 'Chernivtsi, Dovbusha str. 34', '2015-09-01', NULL, '3');
INSERT INTO student VALUES(5, 2, 'Shevchenko', 'Serhii', 'url', 'url', '1998-11-18', 'Chernivtsi, Zelena str. 2', '2015-09-01', NULL, '5');

INSERT INTO student_has_subject VALUES(1, 1);
INSERT INTO student_has_subject VALUES(1, 3);
INSERT INTO student_has_subject VALUES(2, 1);
INSERT INTO student_has_subject VALUES(2, 3);
INSERT INTO student_has_subject VALUES(3, 3);
INSERT INTO student_has_subject VALUES(3, 2);
INSERT INTO student_has_subject VALUES(4, 3);
INSERT INTO student_has_subject VALUES(4, 2);
INSERT INTO student_has_subject VALUES(5, 3);
INSERT INTO student_has_subject VALUES(5, 2);

INSERT INTO grades VALUES(1, 1, 1, 25, 25, 'exam', 90, 4);
INSERT INTO grades VALUES(2, 1, 3, 33, 15, 'credit', 68, 3);
INSERT INTO grades VALUES(3, 2, 1, 32, 25, 'exam', 90, 5);
INSERT INTO grades VALUES(4, 2, 3, 30, 28, 'credit', 91, 5);
INSERT INTO grades VALUES(5, 3, 3, 22, 23, 'credit', 76, 4);
INSERT INTO grades VALUES(6, 3, 2, 26, 15, 'exam', 88, 4);
INSERT INTO grades VALUES(7, 4, 3, 31, 20, 'credit', 74, 4);
INSERT INTO grades VALUES(8, 4, 2, 13, 10, 'exam', 50, 3);
INSERT INTO grades VALUES(9, 5, 3, 20, 22, 'credit', 75, 4);
INSERT INTO grades VALUES(10, 5, 2, 15, 24, 'exam', 60, 3);

	
	
	