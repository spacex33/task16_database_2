# select distinct maker from Product where type='pc' and maker not in (select maker from Product where type = 'Laptop');
# select distinct maker from Product where type='pc' and maker != all(select maker from Product where type='Laptop');
# select distinct maker from product where type='pc' and not maker = any(select maker from product where type='Laptop');
# select distinct maker from product where type='pc' and maker in(select maker from product where type='Laptop');
# select distinct maker from product where type='pc' and maker = any(select maker from product where type="Laptop");

# select distinct maker from product where exists (select model from pc where product.model = model);
# select distinct maker from product where exists (select model from pc where speed>750 and product.model = model);
# select maker from product where exists (select model from pc where product.model = model and speed = (select max(speed) from pc));
# select distinct maker from product where type = 'laptop' and 
#exists (select maker from product where type = 'printer' and maker = product.maker);
# select distinct maker from product where type = 'laptop' and 
#not exsists (select maker from product where type = 'printer' and maker = product.maker);

# select avg(price) 'average price = ' from laptop;
# select concat('code: ', code) code, 
#	concat('model: ', model) model, 
#	concat('speed: ', speed) speed, 
#	concat('ram: ', ram) ram, 
#	concat('hd: ', hd) hd 
#from pc;
# select ship, battle, 
#	case result 
#		when 'sunk' then 'потонув'
#		when 'damaged' then 'пошкоджено'
#		else 'вцілів'
#	end result
#from outcomes;
# select 
#	trip_no, 
#	date, 
#	ID_psg, 
#	concat('row: ', substr(place, 1,1)) 'row',
#	concat('place: ', substr(place, 2,2)) place
#from Pass_in_trip;
#select 
#	trip_no, 
#	ID_comp, 
#	plane, 
#	concat('from ', town_from, ' to ', town_to) from_to, 
#	time_out, 
#	time_in 
#from trip;

# select model, max(distinct price) from printer group by model;
# select distinct model, speed from laptop where speed < (select min(speed) from pc);
# select distinct maker, price from product join printer on product.model = printer.model 
#where price = (select min(price) from printer where color = 'y') and color = 'y';
# select maker, COUNT(model) AS model_count from product group by maker, type having count(model)>2 and type='PC';
# select avg(hd) from product join pc on product.model = pc.model   
#where maker in(select maker from product where type='printer');

# select
#	maker,
#	sum((select count(*) from pc where product.model = pc.model)) pc,
#	sum((select count(*) from laptop where product.model = laptop.model)) laptop,
#	sum((select count(*) from printer where product.model = printer.model)) printer
# from product
#group by maker; 
# select maker, avg(screen) from product, laptop where product.model = laptop.model group by maker;
# select maker, max(price) from product, pc where type = 'pc' and product.model = pc.model group by maker;
# select maker, min(price) from product, pc where type = 'pc' and product.model = pc.model group by maker;
# select speed, avg(price) from pc where speed > 600 group by speed;

# select name, numGuns, bore, displacement, type, launched, classes.class, country
# from ships join classes on ships.class = classes.class
# where
#	case when numGuns = 8 then 1 else 0 end +
#	case when bore = 15 then 1 else 0 end +
#	case when displacement = 32000 then 1 else 0 end +
#	case when type = 'bb' then 1 else 0 end +
#	case when country = 'USA' then 1 else 0 end +
#	case when launched = 1915 then 1 else 0 end +
#	case when classes.class = 'Kongo' then 1 else 0 end > 3;

# select model, price from pc where model = (select model from product where maker = 'B' and type = 'pc')
# union
# select model, price from laptop where model = (select model from product where maker = 'B' and type = 'laptop')
# union
#select model, price from printer where model = (select model from product where maker = 'B' and type = 'printer')

